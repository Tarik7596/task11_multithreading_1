package com.babii.Pipe;

import java.io.IOException;
import java.io.PipedWriter;

public class PipeOut extends Thread {
    PipedWriter out ;
    String msg;

    public PipeOut(PipedWriter out, String msg)
    {
        this.out=out;
        this.msg = msg;
    }

    @Override
    public void run()
    {
        while(true)
        {
            try {
                out.write(msg);
                Thread.sleep(10000);
                System.out.println();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.babii.Tasks;

import java.time.LocalDateTime;

public class Dinner_6 {
    public static void main(String[] args) {
        Thread t1 = new Thread(new SynhronizedRun(), "t1");
        Thread t2 = new Thread(new SynhronizedRun(), "t2");
        Thread t3 = new Thread(new SynhronizedRun(), "t3");
        t1.start();
        t2.start();
        t3.start();
    }

    private Object ob = new Object();

    public void buy() throws InterruptedException {
//        synchronized (this.ob) {
        System.out.println(Thread.currentThread().getName() + " " + LocalDateTime.now());
    }
//    }


    public void cook() throws InterruptedException {
//        synchronized (this.ob) {
        System.out.println(Thread.currentThread().getName() + " " + LocalDateTime.now());
    }
//    }


    public void timePrepared() throws InterruptedException {
//        synchronized (this.ob) {
        System.out.println(Thread.currentThread().getName() + " " + LocalDateTime.now());
    }
//    }
}

class SynhronizedRun extends Thread {
    Dinner_6 dinner_6 = new Dinner_6();
    @Override
    public void run() {
        try {
            dinner_6.buy();

            dinner_6.cook();
            dinner_6.timePrepared();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

package com.babii.Tasks;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Callable_4 {
    public static int fib(int n)
    {
        if(n==1||n==2)
        {
            return 1;
        }
        else
            return fib(n-1)+fib(n-2);
    }
    static Callable<Integer> sumFib(int maxPosition) {
        return () -> {
            int sum = 0;
            for (int i = 1; i < maxPosition; i++) {
                sum += fib(i);

            }
            return sum;
        };
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newWorkStealingPool();
        List<Callable<Integer>> callableList =
                Arrays.asList(sumFib(4),sumFib(7),sumFib(15),sumFib(25));
        executorService.invokeAll(callableList)
                .stream()
                .map(f->{
                    try{
                        return f.get();
                    }     catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                })
                .forEach(System.out::println);
    }
}

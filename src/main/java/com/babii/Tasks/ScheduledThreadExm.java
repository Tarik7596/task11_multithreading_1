package com.babii.Tasks;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadExm {
    public static void main(String[] args) {
        ScheduledExecutorService scheduledE = Executors.newScheduledThreadPool(3);
        Random rand = new Random();
        int r = rand.nextInt(10);
        int t = rand.nextInt(10);
        Task task1 = new Task(1);
        Task task2 = new Task(2);
        Task task3 = new Task(3);
        System.out.println("Time before execute schedule"+LocalDateTime.now());
        scheduledE.schedule(task1, r, TimeUnit.SECONDS);
        scheduledE.schedule(task2, t, TimeUnit.SECONDS);
        scheduledE.schedule(task3,r,TimeUnit.SECONDS);
        try {
            scheduledE.awaitTermination(2,TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Task extends Thread {
    private int taskNum;
    public Task(int taskNum) {
        this.taskNum = taskNum;
    }

    public int getTaskNum() {
        return taskNum;
    }

    @Override
    public void run() {
        try {
            System.out.println("Task№ " +taskNum +"time: "+ LocalDateTime.now());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

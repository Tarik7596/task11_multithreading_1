package com.babii.Tasks;

import java.util.Scanner;

public class FibonacciThread extends Thread {
    private int n;
    public int fib = 1, fibN;

    FibonacciThread(int num) {
        n=num;
    }

    public void run() {
        if(n<=2)
        {
            fibN=1;
        }
        else{
            try{
                FibonacciThread f1 = new FibonacciThread(n-1);
                FibonacciThread f2 = new FibonacciThread(n-2);
                f1.start();
                f2.start();
                f1.join();
                f2.join();
                fibN = f1.fibN + f2.fibN;
            }catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter position of fibonacci number");
        FibonacciThread fibonacciThread = new FibonacciThread(sc.nextInt());
        try {


            fibonacciThread.start();
            fibonacciThread.join();

            System.out.println("The number of fibonacci - "+ " " + fibonacciThread.fibN);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

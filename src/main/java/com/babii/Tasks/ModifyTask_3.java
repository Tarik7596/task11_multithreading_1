package com.babii.Tasks;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ModifyTask_3 extends Thread {
    static ExecutorService executorService = Executors.newSingleThreadExecutor();
    public static int fibN(int n) throws ExecutionException, InterruptedException {

        if (n == 1 || n==2)
            return 1;
        else
            return  fibN(n-1)+fibN(n-2);
    }
    public static int fib_wrapper(int n) throws ExecutionException, InterruptedException {
        Future<Integer> future = executorService.submit(() -> fibN(n-1));
        Future<Integer> future2 = executorService.submit(() -> fibN(n-2));

        return future2.get() + future.get();
    }

    public static void shutdown()
    {
        executorService.submit(() -> {
            for (int i = 3; i < 7; i++) {
                try {
                    System.out.println(fibN(i));
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(() -> {
            for (int i = 9; i < 11; i++) {
                try {
                    System.out.println(fibN(i));
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.shutdownNow();
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        shutdown();
    }
}
